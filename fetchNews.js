const axios = require("axios");
exports.handler = (intention) => {

    return new Promise((resolve, reject) => {
        axios({
                "method": "GET",
                "url": "https://bing-news-search1.p.rapidapi.com/news",
                "headers": {
                    "content-type": "application/octet-stream",
                    "x-rapidapi-host": "bing-news-search1.p.rapidapi.com",
                    "x-rapidapi-key": "9354de278fmsh7df453ccf792716p14f556jsnc5aa1c7c7548",
                    "x-bingapis-sdk": "true",
                    "useQueryString": true
                },
                "params": {
                    "safeSearch": "Off",
                    "textFormat": "Raw"
                }
            })
            .then((axiosResponse) => {
                let news = axiosResponse.data
                let randomNews = news.value[Math.floor(Math.random() * news.value.length)];
                let newsData = {}
                newsData = {
                    newsHeadline: randomNews.name,
                    newsDescription: randomNews.description
                }
                console.log(`Headline:- ${newsData.newsHeadline} \nDescription:-  ${newsData.newsDescription} `);
                resolve(newsData);
            })
            .catch((error) => {
                console.log(error)
            })
    })
}