'use strict'

let axios = require('axios')

exports.handler = (interaction) => {
    return new Promise((resolve, reject) => {
        // Check for parameters
        if (!interaction.parameters.hasOwnProperty('symbol')) {
            reject(new Error('missing symbol parameter for action fetchPriceCryptoCurrency'))
        }

        let symbol = interaction.parameters['symbol']
            //  let symbol = agent.parameters.symbol;
            // Fetch the price of the cryptocurrency
        axios.get(`https://min-api.cryptocompare.com/data/price?fsym=${symbol}&tsyms=USD,EUR,INR`)
            .then(axiosResponse => {
                // Retrieve the prices from the response object, in USD and EUR
                let prices = axiosResponse.data
                let priceData = {}
                    // Check if the API returned an error
                if (prices.Response && prices.Response === 'Error') {
                    // The API returned an error
                    // So build the response object to trigger the failure intent
                    priceData = {
                        name: 'prices-not-found',
                        data: {}
                    }
                } else {
                    // The prices have been successfully retrieved
                    // So build the response object to trigger the success intent
                    priceData = {
                        name: 'prices-found',
                        data: {
                            usd: prices.USD,
                            eur: prices.EUR,
                            inr: prices.INR
                        }
                    }
                }

                // Resolve the Promise to say that the handler performed the action without any error
                resolve(priceData)
            })
            .catch(e => {
                // An error occured during the request to the API
                // Reject the Promise to say that an error occured while the handler was performing the action
                reject(e)
            })


    })
}

function test() {
    let interaction = { parameters: { symbol: 'BTC' } }

    handler(interaction).then(priceData => {
        console.log("==> response", priceData)
    })
}