'use strict'
let axios = require('axios')

exports.handler = (interaction) => {
    console.log(interaction);
    // { parameters: { MovieParam: 'Thor' }, response: {} }

    return new Promise((resolve, reject) => {
        // Check for parameters MovieParam
        if (!interaction.parameters.hasOwnProperty('MovieParam')) {
            reject(new Error('missing Moive parameter for action fetchMovie'))
        }
        let MovieParam = interaction.parameters['MovieParam']
            //  let symbol = agent.parameters.symbol;
            // Fetch the price of the cryptocurrency
        axios.get(`http://www.omdbapi.com/?apikey=cd5d80f0&t=${MovieParam}`)
            .then(axiosResponse => {
                //console.log(axiosResponse)
                // Retrieve the movies from the response object
                let movieDetails = axiosResponse.data
                    //console.log(movieDetails);
                var movieData = {}
                    //console.log(movieData);
                    // Check if the API returned an error
                if (movieDetails.Response && movieDetails.Response === 'Error') {
                    // The API returned an error
                    // So build the response object to trigger the failure intent
                    movieData = {
                        name: 'movie-not-found',
                        data: {}
                    }
                } else {
                    // The prices have been successfully retrieved
                    // So build the response object to trigger the success intent
                    movieData = {
                            name: 'movie-found',
                            data: {
                                movieName: movieDetails.Title,
                                Year: movieDetails.Year,
                                Director: movieDetails.Director,
                                Rating: movieDetails.Ratings[0].Value,
                                Plot: movieDetails.Plot
                            }
                        }
                        //console.log("These are the details found for the movie " + Object.keys(movieData))
                    console.log("These are the details found for the movie " + JSON.stringify(movieData))
                }
                // Resolve the Promise to say that the handler performed the action without any error
                resolve(movieData)
            })
            .catch(e => {
                // An error occured during the request to the API
                // Reject the Promise to say that an error occured while the handler was performing the action
                reject(e)
            })
    })
}