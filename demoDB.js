var mysql = require('mysql');
var express = require('express');
app = express();
//port = process.env.PORT || 3000;

// Create connection here db = connection object
var db = mysql.createConnection({
    host: 'localhost',
    user: 'srk',
    password: 'password',
    database: 'nodemysql'
});

// Connect
db.connect((err) => {
    if (err) throw err;
    console.log("MySQL Connected... ")
});

//Create DB
app.get('/createDB', (req, res) => {
        let sql = 'CREATE DATABASE nodemysql';
        db.query(sql, (err, result) => {
            if (err) throw err
            console.log(result)
            res.send('Database Created')
        })
    })
    //Create Table
app.get('/createTable', (req, res) => {
    let sql = 'CREATE table persons(PersonID int auto_increment, LastName varchar(255), FirstName varchar(255), primary key(PersonID))';
    db.query(sql, (err, result) => {
        if (err) throw err
        console.log(result)
        res.send('Table Created')
    })
})

//Insert values into persons
app.get('/insert1', (req, res) => {
    let person1 = { lastName: 'Vodnala', firstName: 'Srk' }
    let sql = 'insert into persons set ?'
    let query = db.query(sql, person1, (err, result) => {
        if (err) throw err;
        console.log(result);
        res.send('1 row inserted');
    });
})
app.get('/insert2', (req, res) => {
    let person2 = { lastName: 'Gilchrist', firstName: 'Adam' }
    let sql = 'insert into persons set ?'
    let query = db.query(sql, person2, (err, result) => {
        if (err) throw err;
        console.log(result);
        res.send('1 row inserted');
    });
})

// View table
app.get('/persons', (req, res, next) => {
    let sql = 'Select * from persons'
    await db.query(sql, (err, results) => {
        if (err) throw err
        console.log(results)
        res.send(results)
    })
})

//Select single person with id 
app.get('/persons/:personID', (req, res) => {
    let id = req.params.personID
    let sql = `select * from persons where personID=${id}`
    let query = db.query(sql, (err, results) => {
        if (err) throw err
            //  console.log(result);
        var string = JSON.stringify(results);
        console.log('>> string: \n', string);
        var json = JSON.parse(string);
        console.log('>> json: \n', json);
        //console.log('>> LastName: \n', json[0].LastName);
        // req.list = json;
        // next();
        res.send(`User Name is ${JSON.stringify(json[id].LastName)} ${JSON.stringify(json[id].FirstName)}`)
    })
})

// Update person
app.get('/updatePerson/:personID', (req, res) => {
    let newLastName = 'MS'
    let newfirstName = 'Dhoni'
    let sql = `update persons set lastName='${newLastName}', firstName='${newfirstName}' where personID=${req.params.personID}`
    let query = db.query(sql, (err, result) => {
        if (err) throw err
        console.log(result);
        res.send('1 Person Updated')
        res.send(result)
    })
})

//Delete
app.get('/deletePerson/:personID', (req, res) => {
    let sql = `delete from persons where personID=${req.params.personID}`
    let query = db.query(sql, (err, result) => {
        if (err) throw err
        console.log(result);
        res.send('1 Person Updated')
        res.send(result)
    })
})

//db.end();

app.listen(3000, () => {
    console.log("Server running on port 3000")
})