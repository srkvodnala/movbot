const express = require('express')
    // will use this later to send requests
const http = require('http')

const Restify = require("restify");
const server = Restify.createServer({
    name: "MovieBot"
});

// import env variables
require('dotenv').config()

const app = express()
const port = process.env.PORT || 3000

server.use(Restify.plugins.bodyParser());
server.use(Restify.plugins.jsonp());

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.get('/', (req, res) => {
    res.status(200).send('Server is working.')
})

let CCC = require('./fetchPriceCryptoCurrency');
let MMM = require('./fetchMovie');
let NNN = require('./fetchNews');
let inf = require('./basicSql.js');
// POST route handler
server.post("/", (req, res, next) => {
    let { queryResult } = req.body;
    // console.log("==> queryResult: ", queryResult)
    let triggeredIntent = queryResult.intent.displayName;
    console.log("The Name of the triggered intent is " + triggeredIntent);
    if (queryResult.intent.displayName == 'MovieBot') {
        let interaction1 = { parameters: { MovieParam: queryResult.parameters.MovieParam }, response: {} };
        // let sym = queryResult.parameters.symbol;
        MMM.handler(interaction1).then(movieData => {
            console.log("==> Movie JSON response", movieData)
            let respObj = {
                fulfillmentText: `The movie ${movieData.data.movieName} is directed by ${movieData.data.Director} released in the year ${movieData.data.Year} has a rating of ${movieData.data.Rating} and the synopsis is ${movieData.data.Plot}`,
                // fulfillmentText: "The price is (EUR) : " + priceData.data.eur
                // fulfillmentText: "The price is (INR) : " + priceData.data.inr
            };
            // console.log(respObj);
            // console.log(queryResult.intent.displayName);
            res.json(respObj);
            return next()
        })
    } else if (queryResult.intent.displayName == 'Price') {
        let interaction2 = { parameters: { symbol: queryResult.parameters.symbol }, response: {} };
        CCC.handler(interaction2).then(priceData => {
            console.log("==> Price JSON response" + JSON.stringify(priceData.data));
            let resObj = { fulfillmentText: `The price is (EUR) :   ${priceData.data.usd}` }
            res.json(resObj);
            return next();
        })
    } else if (queryResult.intent.displayName == 'NewsBot') {
        let interaction3 = { res: {} };
        // console.log(res);
        NNN.handler(interaction3).then(newData => {
            // Here the function name can be different. 
            // console.log(JSON.stringify(newData));
            let newsRes = { fulfillmentText: `Headline:- ${newData.newsHeadline} \n \n
            Description:-  ${newData.newsDescription} ` }
            res.json(newsRes);

            let content = newData.newsDescription;

            inf.writeToHistory(triggeredIntent, content);
            inf.getHistory();

            return next();
        })




    } else if (queryResult.intent.displayName == 'userDetails') {

        let interaction4 = { parameters: { info: queryResult.parameters.info, id: queryResult.parameters.id }, response: {} };
        //console.log('Interaction 4 Response ' + JSON.stringify(interaction4.parameters));

        if (interaction4.parameters.id != '') {
            inf.select(interaction4).then(userData => {
                console.log("==> User JSON response, ", userData)
                let resObj = { fulfillmentText: `The user Name is :  ${userData[0].FirstName} ${userData[0].LastName} ` }
                console.log(resObj);
                res.json(resObj);
                return next();
            })
        } else {
            inf.handler(interaction4).then(userData => {
                console.log("@@@@@@@@@@@@@@@@@@@   sql Handler     @@@@@@@@@@@@@@@@@@")
                console.log(userData)
                let resObj = { fulfillmentText: `The user ${userData[0].PersonID} Name is :  ${userData[0].FirstName} ${userData[0].LastName} \n The user  ${userData[1].PersonID} Name is :  ${userData[1].FirstName} ${userData[1].LastName} ` }
                console.log(resObj);
                res.json(resObj);
                return next();
            })
        }
    }

});



server.listen(port, () => {
    console.log(`🌏 Server is running at http://localhost:${port}`)
})